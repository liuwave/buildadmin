<?php

class ParseDownExt extends ParsedownExtra
{
    protected function inlineLink($Excerpt)
    {
        $link = parent::inlineLink($Excerpt);
        if (!isset($link)) {
            return null;
        }
        $link['element']['attributes']['target'] = '_blank';
        return $link;
    }
}