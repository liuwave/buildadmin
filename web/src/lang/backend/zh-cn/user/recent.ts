export default {
    id: 'ID',
    user_id: '会员',
    user__username: '用户名',
    content: '动态详情',
    weigh: '权重',
    status: '状态',
    'status 0': '禁用',
    'status 1': '启用',
    create_time: '创建时间',
    'quick Search Fields': 'ID',
}
