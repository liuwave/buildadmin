export default {
    id: 'ID',
    user_id: '发送人',
    user__username: '用户名',
    recipient_id: '接受人',
    content: '消息内容',
    status: '状态',
    'status unread': '未读',
    'status read': '已读',
    create_time: '创建时间',
    'quick Search Fields': 'ID、消息内容',
}
