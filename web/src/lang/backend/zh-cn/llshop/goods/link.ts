export default {
    id: 'ID',
    link: '商品链接',
    promotion_link: '推荐URL',
    promotion_link_short: '推荐短链',
    coupon_link: '优惠券链接',
    create_time: '创建时间',
    update_time: '修改时间',
    'quick Search Fields': 'ID',
}
