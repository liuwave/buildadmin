export default {
    id: 'ID',
    name: '模型名称',
    table: '表名',
    info: '详情页模板',
    status: '状态',
    'status 0': '禁用',
    'status 1': '启用',
    update_time: '修改时间',
    create_time: '创建时间',
    'quick Search Fields': 'ID、名称',
    'model field': '模型字段管理',
}
