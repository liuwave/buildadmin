export default {
    id: 'ID',
    user_id: '会员',
    user__username: '用户名',
    object_id: '付款对象',
    cms_content__title: '付款对象标题',
    title: '标题',
    project: '项目',
    'project admire': '赞赏',
    'project content': '内容付费',
    amount: '金额',
    type: '支付方式',
    'type wx': '微信',
    'type alipay': '支付宝',
    'type balance': '余额',
    'type score': '积分',
    sn: '支付平台订单号',
    remark: '备注',
    pay_time: '支付时间',
    create_time: '创建时间',
    'quick Search Fields': 'ID、标题',
}
