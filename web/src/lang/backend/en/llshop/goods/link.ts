export default {
    id: 'id',
    link: 'link',
    promotion_link: 'promotion_link',
    promotion_link_short: 'promotion_link_short',
    coupon_link: 'coupon_link',
    create_time: 'create_time',
    update_time: 'update_time',
    'quick Search Fields': 'id',
}
