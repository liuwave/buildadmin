export default {
    id: 'id',
    title: 'title',
    position: 'position',
    'position statistic': 'position statistic',
    'position under_motto': 'position under_motto',
    'position opt': 'position opt',
    'position middle': 'position middle',
    'position tab': 'position tab',
    // tips
    'statistic tips': 'As one of the member statistics lines',
    'under_motto tips':
        'This type of component is placed between the personality signature and the statistics, and each component has an exclusive line',
    'opt tips': 'As one of the items in the action button line under statistics',
    'middle tips':
        'This type of component will be placed between the member information section and the TAB section, with each component having an exclusive line',
    'tab tips': 'As one of the bottom tabs, the title will be the label of the marquee card',
    component: 'component',
    'component tips': 'Just fill out the component name, please place the component in /web-nuxt/composables/template/interaction folder',
    weigh: 'weigh',
    create_time: 'create_time',
    'quick Search Fields': 'id',
    'card tips 1': 'Front-end member business card page, is composed of a number of custom components',
    'card tips 2': 'You can manage various components of the member business card page here to adjust the display effect of the member business card',
    'card tips 3':
        'Components need to be developed in advance by the developer and then configured here where the component should be displayed, etc',
}
