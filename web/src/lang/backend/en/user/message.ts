export default {
    id: 'id',
    user_id: 'user_id',
    user__username: 'username',
    recipient_id: 'recipient_id',
    content: 'content',
    status: 'status',
    'status unread': 'status unread',
    'status read': 'status read',
    create_time: 'create_time',
    'quick Search Fields': 'content,id',
}
