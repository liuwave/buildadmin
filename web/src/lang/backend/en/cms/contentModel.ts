export default {
    id: 'id',
    name: 'name',
    table: 'table',
    info: 'info',
    status: 'status',
    'status 0': 'status 0',
    'status 1': 'status 1',
    update_time: 'update_time',
    create_time: 'create_time',
    'quick Search Fields': 'id、name',
    'model field': 'Model field',
}
