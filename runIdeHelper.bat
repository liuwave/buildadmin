
:: php think ide-helper:model app\\model\\User app\\model\\Post

:: --dir="models" [-D] 指定自动搜索模型的目录,相对于应用基础目录的路径，可指定多个，默认为app/model

:: --ignore="app\\model\\User,app\\model\\Post" [-I] 忽略的模型，可指定多个

:: --overwrite [-O] 强制覆盖已有的属性注释

:: --reset [-R] 重置模型的所有的注释

::php think ide-helper:model --dir="admin/model/cms"
::php think ide-helper:model --dir="admin/model/user"


:: php think ide-helper:model --dir="admin/model" -I "app\admin\model\Admin"
php think ide-helper:model --dir="admin/model"
