<?php

namespace app\admin\controller\cms;

use app\common\controller\Backend;
use Throwable;

/**
 * 用户支付记录
 *
 */
class PayLog extends Backend
{
    /**
     * PayLog模型对象
     * @var object
     * @phpstan-var \app\admin\model\cms\PayLog
     */
    protected object $model;

    protected string|array $preExcludeFields = ['id', 'pay_time', 'create_time'];

    protected array $withJoinTable = ['user', 'cmsContent'];

    protected string|array $quickSearchField = ['id', 'title'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\cms\PayLog;
    }

    /**
     * 查看
     * @throws Throwable
     */
    public function index(): void
    {
        $this->request->filter(['strip_tags', 'trim']);
        // 如果是select则转发到select方法,若select未重写,其实还是继续执行index
        if ($this->request->param('select')) {
            $this->select();
        }

        list($where, $alias, $limit, $order) = $this->queryBuilder();
        $res = $this->model
            ->withJoin($this->withJoinTable, $this->withJoinType)
            ->alias($alias)
            ->where($where)
            ->order($order)
            ->paginate($limit);
        $res->visible(['user' => ['username'], 'cmsContent' => ['title']]);

        $this->success('', [
            'list'   => $res->items(),
            'total'  => $res->total(),
            'remark' => get_route_remark(),
        ]);
    }
}