<?php

namespace app\admin\controller\cms;

use app\common\controller\Backend;

/**
 * 标签管理
 *
 */
class Tags extends Backend
{
    /**
     * Tags模型对象
     * @var object
     * @phpstan-var \app\admin\model\cms\Tags
     */
    protected object $model;

    protected string|array $defaultSortField = 'weigh,desc';

    protected string|array $preExcludeFields = ['update_time', 'create_time'];

    protected string|array $quickSearchField = ['id'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\cms\Tags;
    }
}