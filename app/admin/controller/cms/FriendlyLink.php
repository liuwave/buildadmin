<?php

namespace app\admin\controller\cms;

use app\common\controller\Backend;

/**
 * 友情链接
 *
 */
class FriendlyLink extends Backend
{
    /**
     * FriendlyLink模型对象
     * @var object
     * @phpstan-var \app\admin\model\cms\FriendlyLink
     */
    protected object $model;

    protected string|array $defaultSortField = 'weigh,desc';

    protected string|array $preExcludeFields = ['id', 'update_time', 'create_time'];

    protected array $withJoinTable = ['user'];

    protected string|array $quickSearchField = ['title', 'id'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\cms\FriendlyLink;
    }

    /**
     * 查看
     */
    public function index(): void
    {
        $this->request->filter(['strip_tags', 'trim']);
        // 如果是select则转发到select方法,若select未重写,其实还是继续执行index
        if ($this->request->param('select')) {
            $this->select();
        }

        list($where, $alias, $limit, $order) = $this->queryBuilder();
        $res = $this->model
            ->withJoin($this->withJoinTable, $this->withJoinType)
            ->alias($alias)
            ->where($where)
            ->order($order)
            ->paginate($limit);
        $res->visible(['user' => ['username']]);

        $this->success('', [
            'list'   => $res->items(),
            'total'  => $res->total(),
            'remark' => get_route_remark(),
        ]);
    }
}