<?php

namespace app\admin\library\traits;

use think\facade\Cache;

trait DbCache
{

    public static function clearCache($tags = []): void
    {
        if (property_exists(static::class, 'cacheTag')) {

            $tags = empty($tags) ? self::$cacheTag : $tags;
            Cache::store(config('database.cache_store'))
                ->tag($tags)->clear();
        }

    }

}