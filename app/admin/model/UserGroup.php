<?php

namespace app\admin\model;

use think\Model;

/**
 * UserGroup 模型
 *
 * @property int $id ID
 * @property string $create_time 创建时间
 * @property string $name 组名
 * @property string $rules 权限节点
 * @property string $status 状态:0=禁用,1=启用
 * @property string $update_time 更新时间
 */
class UserGroup extends Model
{
    protected $autoWriteTimestamp = true;
}