<?php

namespace app\admin\model;

use think\model;

/**
 * UserRule 模型
 *
 * @property int $id ID
 * @property int $no_login_valid 未登录有效:0=否,1=是
 * @property int $pid 上级菜单
 * @property int $weigh 权重
 * @property string $component 组件路径
 * @property string $create_time 创建时间
 * @property string $extend 扩展属性:none=无,add_rules_only=只添加为路由,add_menu_only=只添加为菜单
 * @property string $icon 图标
 * @property string $menu_type 菜单类型:tab=选项卡,link=链接,iframe=Iframe
 * @property string $name 规则名称
 * @property string $path 路由路径
 * @property string $remark 备注
 * @property string $status 状态:0=禁用,1=启用
 * @property string $title 标题
 * @property string $type 类型:route=路由,menu_dir=菜单目录,menu=菜单项,nav_user_menu=顶栏会员菜单下拉项,nav=顶栏菜单项,button=页面按钮
 * @property string $update_time 更新时间
 * @property string $url Url
 */
class UserRule extends model
{
    protected $autoWriteTimestamp = true;


    public static string $cacheTag = 'user_rule';

    protected static function onAfterInsert($model)
    {
        $pk = $model->getPk();
        $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
    }

    public function setComponentAttr($value)
    {
        if ($value) $value = str_replace('\\', '/', $value);
        return $value;
    }
}