<?php

namespace app\admin\model;

use think\Model;

/**
 * Log
 *
 * @property array $fields 字段数据
 * @property array $table 数据表数据
 * @property int $id ID
 * @property string $create_time 创建时间
 * @property string $status 状态:delete=已删除,success=成功,error=失败,start=生成中
 * @property string $table_name 数据表名
 */
class CrudLog extends Model
{
    // 表名
    protected $name = 'crud_log';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    protected $type = [
        'table'  => 'array',
        'fields' => 'array',
    ];

}