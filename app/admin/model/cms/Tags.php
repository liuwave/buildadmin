<?php

namespace app\admin\model\cms;

use think\Model;

/**
 * Tags
 *
 * @property int $document_count 文档数
 * @property int $id ID
 * @property int $inner_chain 自动内链:0=关,1=开
 * @property int $status 状态:0=禁用,1=启用
 * @property int $weigh 权重
 * @property string $create_time 创建时间
 * @property string $description 描述
 * @property string $keywords 关键词
 * @property string $name 名称
 * @property string $seotitle SEO标题
 * @property string $type 标签类型:default=default,success=success,info=info,warning=warning,danger=danger
 * @property string $update_time 修改时间
 */
class Tags extends Model
{
    // 表名
    protected $name = 'cms_tags';

    public static string $cacheTag = 'cms_tags';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

}