<?php

namespace app\admin\model\cms;

use app\admin\library\traits\DbCache;
use think\facade\Cache;
use think\Model;

/**
 * Block
 *
 * @property int $id ID
 * @property int $status 状态:0=禁用,1=启用
 * @property int $weigh 权重
 * @property string $create_time 创建时间
 * @property string $end_time 结束时间
 * @property string $image 图片
 * @property string $link 链接
 * @property string $name 名称
 * @property string $rich_text 富文本
 * @property string $start_time 开始时间
 * @property string $target 跳转方式:_blank=新标签页,_self=当前标签页,_top=当前窗体,_parent=父窗口
 * @property string $title 标题
 * @property string $type 类型:carousel=轮播图,image=图片,rich_text=富文本
 * @property string $update_time 修改时间
 */
class Block extends Model
{
    use DbCache;
    // 表名
    protected $name = 'cms_block';


    public static string $cacheTag='cms_block';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 字段类型转换
    protected $type = [
        'start_time' => 'timestamp:Y-m-d H:i:s',
        'end_time'   => 'timestamp:Y-m-d H:i:s',
    ];


    protected static function onAfterInsert($model): void
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            if (strlen($model[$pk]) >= 19) {
                $model->where($pk, $model[$pk])->update(['weigh' => $model->count()]);
            } else {
                $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
            }
        }
        self::clearCache();
    }

    protected static function onAfterUpdate($model): void
    {
        self::clearCache();
    }



    public function getRichTextAttr($value): string
    {
        return !$value ? '' : htmlspecialchars_decode($value);
    }

    public function  getLinkAttr($value): string
    {
        return !$value ? '' : htmlspecialchars_decode($value);
    }
}