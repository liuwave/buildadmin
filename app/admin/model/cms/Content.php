<?php

namespace app\admin\model\cms;

use app\admin\library\traits\DbCache;
use app\admin\model\Admin;
use app\admin\model\User;
use modules\cms\library\Helper;
use think\facade\Db;
use think\Model;
use think\model\relation\BelongsTo;

/**
 * Content
 *
 * @property float $price 价格
 * @property int $admin_id 发布管理员
 * @property int $channel_id 频道
 * @property int $comments 评论量
 * @property int $dislikes 点踩数
 * @property int $id ID
 * @property int $likes 点赞数
 * @property int $user_id 投稿会员
 * @property int $views 浏览量
 * @property int $weigh 权重
 * @property string $allow_comment_groups 评论:disable=禁止,all=全部,user=仅会员
 * @property string $allow_visit_groups 允许访问:all=全部,user=仅会员
 * @property string $channel_ids 副频道
 * @property string $content 内容
 * @property string $create_time 创建时间
 * @property string $currency 货币:RMB=人民币,integral=积分
 * @property string $description 描述
 * @property string $flag 标志:top=置顶,hot=热门,recommend=推荐,new=最新
 * @property string $images 封面图片
 * @property string $keywords 关键词
 * @property string $memo 拒绝稿件原因
 * @property string $publish_time 发布时间
 * @property string $seotitle SEO标题
 * @property string $status 状态:normal=正常,unaudited=待审核,refused=已拒绝,offline=已下线
 * @property string $tags 标签
 * @property string $target 跳转方式:_blank=新标签页,_self=当前标签页,_top=当前窗体,_parent=父窗口
 * @property string $title 标题
 * @property string $title_style 标题样式
 * @property string $update_time 修改时间
 * @property string $url 外部链接
 * @property-read \app\admin\model\Admin $admin
 * @property-read \app\admin\model\User $user
 * @property-read \app\admin\model\cms\Channel $cms_channel
 * @property-read mixed $cms_tags
 */
class Content extends Model
{

    use DbCache;

    // 表名
    protected $name = 'cms_content';

    public static string $cacheTag = 'cms_content';

    public static function getCacheKey($name = ''): string
    {
        return self::$cacheTag . $name;
    }

    public static function getCacheIdTag($id = ''): string
    {
        return self::$cacheTag . $id;
    }


    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 追加属性
    protected $append = [
        'cmsChannel',
        'cmsTags',
    ];

    // 字段类型转换
    protected $type = [
        'publish_time' => 'timestamp:Y-m-d H:i:s',
    ];

    protected string $contentLanguage = '';

    public function __construct(array $data = [])
    {
        parent::__construct($data);

//        $this->contentLanguage = Db::name('cms_config')
//            ->where('name', 'content_language')
//            ->value('value');
        $this->contentLanguage = Config::getCmsConfig('content_language');
    }


    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
        }
    }


    public static function getTitleStyleAttr($value): array
    {
        if (!$value) return [];
        $value = json_decode($value, true);
        if ($value['bold']) $value['bold'] = true;
        return $value;
    }

    public function setTitleStyleAttr($value): string
    {
        if ($value) {
            if ($value['bold']) $value['bold'] = 1;
            return json_encode($value);
        }
        return '';
    }

    public function getChannelIdsAttr($value): array
    {
        if ($value === '' || $value === null) return [];
        if (!is_array($value)) {
            return explode(',', $value);
        }
        return $value;
    }

    public function setChannelIdsAttr($value): string
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    public function getFlagAttr($value): array
    {
        if ($value === '' || $value === null) return [];
        if (!is_array($value)) {
            return explode(',', $value);
        }
        return $value;
    }

    public function setFlagAttr($value): string
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    public static function getImagesAttr($value): array
    {
        if ($value === '' || $value === null) return [];
        if (!is_array($value)) {
            $images = explode(',', $value);
            return array_map(function ($image) {
                return full_url($image);
            }, array_filter($images));
        }
        return $value;
    }

    public function setImagesAttr($value): string
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    public function getContentAttr($value): string
    {
        $value = !$value ? '' : htmlspecialchars_decode($value);
        if ($this->contentLanguage == 'html') {
            if (!str_contains($value, '<p>')) {
                $value = '<p>' . $value . '</p>';
            }
        }
        return $value;
    }

    public function getCmsTagsAttr($value, $row): array
    {
        return isset($row['tags']) ? [
            'name' => \app\admin\model\cms\Tags::cacheAlways(Helper::getDbCacheEnabled(), 3600, Tags::$cacheTag)
                ->whereIn('id', $row['tags'])->column('name', 'id'),
        ] : [];
    }

    public function getAuthorInfoAttr($value, $row): User|Admin|null
    {
        if (isset($row['user_id']) && $row['user_id']) {

            return User::field('id,nickname,avatar,motto')
                ->cacheAlways(Helper::getDbCacheEnabled()?'author_info_user'.$row['user_id']:false, 3600)->where('id', $row['user_id'])->find();

        } elseif (isset($row['admin_id']) && $row['admin_id']) {
            return Admin::field('id,nickname,avatar,motto')
                ->cacheAlways(Helper::getDbCacheEnabled()?'author_info_admin'.$row['admin_id']:false, 3600)->where('id', $row['admin_id'])->find();
        }


        return null;
    }

    public function getTagsAttr($value): array
    {
        if ($value === '' || $value === null) return [];
        if (!is_array($value)) {
            return explode(',', $value);
        }
        return $value;
    }

    public function setTagsAttr($value): string
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    public function getPriceAttr($value): float
    {
        return (float)$value;
    }

    public static function modelDataOutput($value, $type): mixed
    {
        if (in_array($type, ['checkbox', 'images', 'files'])) {
            if ($value === '' || $value === null) return [];
            if (!is_array($value)) {
                return explode(',', $value);
            }
            return $value;
        } elseif ($type == 'array') {
            return empty($value) ? [] : json_decode($value, true);
        } elseif ($type == 'editor') {
            return !$value ? '' : htmlspecialchars_decode($value);
        } elseif ($type == 'city') {
            if ($value === '' || $value === null) return '';
            $cityNames = Db::name('area')->whereIn('id', $value)->column('name');
            return $cityNames ? implode(',', $cityNames) : '';
        } elseif ($type == 'datetime') {
            return $value ? date('Y-m-d H:i:s', $value) : '';
        } else {
            return $value;
        }
    }

    public static function getModelTableData($value, $type): mixed
    {
        if (!$type) return $value;
        if ($type == 'array') {
            return empty($value) ? [] : json_decode($value, true);
        } elseif ($type == 'switch') {
            return (bool)$value;
        } elseif ($type == 'datetime') {
            return $value ? date('Y-m-d H:i:s', $value) : '';
        } elseif ($type == 'editor') {
            return !$value ? '' : htmlspecialchars_decode($value);
        } elseif (in_array($type, ['city', 'checkbox', 'selects', 'remoteSelects'])) {
            if ($value == '') {
                return [];
            }
            if (!is_array($value)) {
                return explode(',', $value);
            }
            return $value;
        }
        return $value;
    }

    public static function setModelTableData($value, $type): string|int|bool
    {
        if ($type == 'array') {
            return $value ? json_encode($value) : '';
        } elseif ($type == 'switch') {
            return $value ? '1' : '0';
        } elseif ($type == 'time') {
            return $value ? date('H:i:s', strtotime($value)) : '';
        } elseif ($type == 'datetime') {
            return $value ? strtotime($value) : '';
        } elseif ($type == 'city' || $type == 'checkbox' || $type == 'selects') {
            if ($value && is_array($value)) {
                return implode(',', $value);
            }
            return $value ?: '';
        } elseif (is_array($value)) {
            return implode(',', $value);
        }
        return $value;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\Admin::class, 'admin_id', 'id');
    }

    public function cmsChannel(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\cms\Channel::class, 'channel_id', 'id')
            ->cacheAlways(Helper::getDbCacheEnabled(), null, Channel::$cacheTag);
    }
}