<?php

namespace app\admin\model\cms;

use app\admin\library\traits\DbCache;
use modules\cms\library\Helper;
use think\facade\Cache;
use think\Model;
use think\model\relation\BelongsTo;

/**
 * Channel
 *
 * @property int $content_model_id 内容模型
 * @property int $frontend_contribute 允许投稿:0=关,1=开
 * @property int $id ID
 * @property int $pid 上级频道
 * @property int $status 状态:0=禁用,1=启用
 * @property int $weigh 权重
 * @property string $allow_visit_groups 访问策略:all=全部,user=仅会员
 * @property string $create_time 创建时间
 * @property string $description 描述
 * @property string $index_rec 推荐到首页
 * @property string $keywords 关键词
 * @property string $name 频道名称
 * @property string $seotitle SEO标题
 * @property string $target 跳转方式:_blank=新标签页,_self=当前标签页,_top=当前窗体,_parent=父窗口
 * @property string $template 首页模板
 * @property string $type 类型:cover=封面频道,list=列表频道,link=跳转链接
 * @property string $update_time 修改时间
 * @property string $url 跳转链接
 * @property-read \app\admin\model\cms\Channel $cms_channel
 * @property-read \app\admin\model\cms\ContentModel $cms_content_model
 */
class Channel extends Model
{
    use DbCache;

    // 表名
    protected $name = 'cms_channel';


    public static string $cacheTag = 'cms_channel';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;


    public static function getCacheKey($name = ''): string
    {
        return self::$cacheTag . $name;
    }

    protected static function onAfterInsert($model): void
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
        }
        self::clearCache();
    }

    protected static function onAfterUpdate($model): void
    {
        self::clearCache();
    }


    public function cmsChannel(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\cms\Channel::class, 'pid', 'id')
            ->cache(Helper::getDbCacheEnabled(), null, self::$cacheTag);
    }

    public function cmsContentModel(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\cms\ContentModel::class, 'content_model_id', 'id')
            ->cache(Helper::getDbCacheEnabled(), null, ContentModel::$cacheTag);
    }
}