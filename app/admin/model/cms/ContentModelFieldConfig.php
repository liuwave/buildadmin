<?php

namespace app\admin\model\cms;

use app\admin\library\traits\DbCache;
use think\Model;
use think\model\relation\BelongsTo;

/**
 * ContentModelFieldConfig
 *
 * @property int $backend_com_search 公共搜索:0=否,1=是
 * @property int $backend_publish 发布可用:0=否,1=是
 * @property int $backend_show 表格显示:0=关,1=开
 * @property int $backend_sort 排序:0=否,1=是
 * @property int $content_model_id 内容模型ID
 * @property int $frontend_contribute 前台投稿:0=否,1=是
 * @property int $frontend_filter 前台筛选:0=否,1=是
 * @property int $id ID
 * @property int $status 状态:0=禁用,1=启用
 * @property int $weigh 权重
 * @property string $backend_column_attr 表格列属性
 * @property string $create_time 创建时间
 * @property string $extend formitem 扩展信息
 * @property string $frontend_filter_dict 前台筛选字典
 * @property string $input_extend input 扩展信息
 * @property string $name 名称
 * @property string $rule 验证规则
 * @property string $tip 提示信息
 * @property string $title 标题
 * @property string $type 类型
 * @property string $update_time 修改时间
 * @property-read \app\admin\model\cms\ContentModel $cms_content_model
 */
class ContentModelFieldConfig extends Model
{
    // 表名
    protected $name = 'cms_content_model_field_config';

    public static string $cacheTag = 'cms_content_model_field_config';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    use DbCache;

    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk       = $model->getPk();
            $maxWeigh = $model->max('weigh');
            $model->where($pk, $model[$pk])->update(['weigh' => $maxWeigh + 1]);
        }
    }

    public function setRuleAttr($value): string
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    public function getRuleAttr($value): array
    {
        if ($value === '' || $value === null) return [];
        if (!is_array($value)) {
            return explode(',', $value);
        }
        return $value;
    }

    public function cmsContentModel(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\cms\ContentModel::class, 'content_model_id', 'id');
    }
}