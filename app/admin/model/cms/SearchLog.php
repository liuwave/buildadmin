<?php

namespace app\admin\model\cms;

use think\Model;
use think\model\relation\BelongsTo;

/**
 * SearchLog
 *
 * @property int $count 搜索次数
 * @property int $hot 热门搜索:0=关,1=开
 * @property int $id ID
 * @property int $status 状态:0=禁用,1=启用
 * @property int $user_id 添加用户
 * @property int $weigh 权重
 * @property string $create_time 创建时间
 * @property string $search 关键词
 * @property-read \app\admin\model\User $user
 */
class SearchLog extends Model
{
    // 表名
    protected $name = 'cms_search_log';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            if (strlen($model[$pk]) >= 19) {
                $model->where($pk, $model[$pk])->update(['weigh' => $model->count()]);
            } else {
                $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
            }
        }
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }
}