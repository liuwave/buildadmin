<?php

namespace app\admin\model\cms;

use think\Model;
use think\model\relation\BelongsTo;

/**
 * FriendlyLink
 *
 * @property int $id ID
 * @property int $user_id 会员
 * @property int $weigh 权重
 * @property string $create_time 创建时间
 * @property string $link 链接
 * @property string $logo LOGO
 * @property string $remark 备注
 * @property string $status 状态:disable=禁用,enable=启用,pending_trial=待审
 * @property string $target 跳转方式:_blank=新标签页,_self=当前标签页,_top=当前窗体,_parent=父窗口
 * @property string $title 标题
 * @property string $update_time 修改时间
 * @property-read \app\admin\model\User $user
 */
class FriendlyLink extends Model
{
    // 表名
    protected $name = 'cms_friendly_link';


    public static string $cacheTag = 'cms_friendly_link';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            if (strlen($model[$pk]) >= 19) {
                $model->where($pk, $model[$pk])->update(['weigh' => $model->count()]);
            } else {
                $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
            }
        }
    }

    public function getLogoAttr($value): string
    {
        return full_url($value);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }
}