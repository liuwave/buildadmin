<?php

namespace app\admin\model\cms;

use app\admin\library\traits\DbCache;
use think\facade\Cache;
use think\Model;

/**
 * ContentModel
 *
 * @property int $id ID
 * @property int $status 状态:0=禁用,1=启用
 * @property string $create_time 创建时间
 * @property string $info 详情页模板
 * @property string $name 模型名称
 * @property string $table 表名
 * @property string $update_time 修改时间
 */
class ContentModel extends Model
{
    // 表名
    protected $name = 'cms_content_model';

    public static string $cacheTag = 'cms_content_model';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    use DbCache;




}