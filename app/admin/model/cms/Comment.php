<?php

namespace app\admin\model\cms;

use app\admin\library\traits\DbCache;
use think\facade\Db;
use think\Model;
use think\model\relation\BelongsTo;

/**
 * Comment
 *
 * @property array $at_user AT用户列表
 * @property int $content_id 评论对象
 * @property int $id ID
 * @property int $user_id 会员
 * @property int $weigh 权重
 * @property string $content 富文本
 * @property string $create_time 创建时间
 * @property string $remark 备注
 * @property string $status 状态:normal=正常,unaudited=待审核,refused=已拒绝
 * @property string $type 类型:content=内容评论,page=单页评论
 * @property-read \app\admin\model\User $user
 * @property-read \app\admin\model\cms\Content $cms_content
 */
class Comment extends Model
{
    // 表名
    protected $name = 'cms_comment';

    public static  string $cacheTag = 'cms_comment';


    use DbCache;
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    protected $type = [
        'at_user' => 'array',
    ];

    protected string $commentLanguage = '';

    public function __construct(array $data = [])
    {
        parent::__construct($data);
//        $this->commentLanguage = Db::name('cms_config')
//            ->where('name', 'comment_language')
//            ->value('value');
        $this->commentLanguage=Config::getCmsConfig('comment_language');
    }

    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            if (strlen($model[$pk]) >= 19) {
                $model->where($pk, $model[$pk])->update(['weigh' => $model->count()]);
            } else {
                $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
            }
        }

        self::clearCache();
    }

    public function getContentAttr($value): string
    {
        $value = !$value ? '' : htmlspecialchars_decode($value);
        if ($this->commentLanguage == 'html') {
            if (!str_contains($value, '<p>')) {
                $value = '<p>' . $value . '</p>';
            }
        }
        return $value;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }

    public function cmsContent(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\cms\Content::class, 'content_id', 'id');
    }
}