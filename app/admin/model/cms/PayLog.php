<?php

namespace app\admin\model\cms;

use think\Model;
use think\model\relation\BelongsTo;

/**
 * PayLog
 *
 * @property int $amount 金额
 * @property int $id ID
 * @property int $object_id 付款对象
 * @property int $user_id 会员
 * @property string $create_time 创建时间
 * @property string $pay_time 支付时间
 * @property string $project 项目:admire=赞赏,content=内容付费
 * @property string $remark 备注
 * @property string $sn 支付平台订单号
 * @property string $title 标题
 * @property string $type 支付方式:wx=微信,alipay=支付宝,balance=余额,score=积分
 * @property-read \app\admin\model\User $user
 * @property-read \app\admin\model\cms\Content $cms_content
 */
class PayLog extends Model
{
    // 表名
    protected $name = 'cms_pay_log';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    // 字段类型转换
    protected $type = [
        'pay_time' => 'timestamp:Y-m-d H:i:s',
    ];

    public function getAmountAttr($value): float
    {
        return (float)bcdiv($value, 100, 2);
    }

    public function setAmountAttr($value): string
    {
        return bcmul($value, 100);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }

    public function cmsContent(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\cms\Content::class, 'object_id', 'id');
    }
}