<?php

namespace app\admin\model\cms;

use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Cache;
use think\Model;

/**
 * Class app\admin\model\cms\Config
 *
 * @property int $id
 * @property string $group 分组
 * @property string $name 变量名
 * @property string $title 变量标题
 * @property string $type 类型
 * @property string $value 变量值
 */
class Config extends Model
{
    public static string $cacheTag = 'cms_config';

    protected $name = 'cms_config';


    public static function getCmsConfig(string $name = '', mixed $default = null, array $where=[], bool $consise = true): mixed
    {
        if ($name) {
            $config = self::cache(self::$cacheTag . $name, null, self::$cacheTag)->where('name', $name)->find();
            if (!$config) {
                return $default;
            }
            return $config['value'];
        } else {
            $config = self::cache(self::$cacheTag . 'cache_all'.serialize($where), null, self::$cacheTag)
                ->where($where)
                ->select();

            if ($consise) {
                $data = [];
                foreach ($config as $item) {
                    $data[$item['name']] = $item['value'];
                }
                return $data;
            }
            return $config;
        }
    }

    public static function clearCmsConfigCache(): void
    {
        Cache::store(\think\facade\Config::get('database.cache_store'))
            ->tag(self::$cacheTag)
            ->clear();
    }


}