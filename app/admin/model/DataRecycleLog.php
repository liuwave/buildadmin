<?php

namespace app\admin\model;

use think\Model;
use think\model\relation\BelongsTo;

/**
 * DataRecycleLog 模型
 *
 * @property int $admin_id 操作管理员
 * @property int $id ID
 * @property int $is_restore 是否已还原:0=否,1=是
 * @property int $recycle_id 回收规则ID
 * @property string $create_time 创建时间
 * @property string $data 回收的数据
 * @property string $data_table 数据表
 * @property string $ip 操作者IP
 * @property string $primary_key 数据表主键
 * @property string $useragent User-Agent
 * @property-read \app\admin\model\Admin $admin
 * @property-read \app\admin\model\DataRecycle $recycle
 */
class DataRecycleLog extends Model
{
    protected $name = 'security_data_recycle_log';

    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    public function recycle(): BelongsTo
    {
        return $this->belongsTo(DataRecycle::class, 'recycle_id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}