<?php

namespace app\admin\model\user;

use Throwable;
use think\Model;
use think\model\relation\BelongsTo;

/**
 * Recent
 *
 * @property int $id ID
 * @property int $status 状态:0=禁用,1=启用
 * @property int $user_id 会员
 * @property int $weigh 权重
 * @property string $content 动态详情
 * @property string $pure_content 纯文本
 * @property string $source_type 关联类型 'default','publish_content','publish_comment','message'
 * @property int $source_id 关联ID
 * @property string $create_time 创建时间
 * @property-read \app\admin\model\User $user
 */
class Recent extends Model
{
    // 表名
    protected $name = 'user_recent';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    /**
     * 入库前
     * @throws Throwable
     */
    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            if (strlen($model[$pk]) >= 19) {
                $model->where($pk, $model[$pk])->update(['weigh' => $model->count()]);
            } else {
                $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
            }
        }
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }
}