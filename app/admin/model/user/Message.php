<?php

namespace app\admin\model\user;

use think\Model;
use think\model\relation\BelongsTo;

/**
 * Message
 *
 * @property int $del_user_id 消息删除状态
 * @property int $id ID
 * @property int $recipient_id 接受人
 * @property int $user_id 发送人
 * @property string $content 消息内容
 * @property string $create_time 创建时间
 * @property string $status 状态:unread=未读,read=已读
 * @property-read \app\admin\model\User $recipient
 * @property-read \app\admin\model\User $user
 */
class Message extends Model
{
    // 表名
    protected $name = 'user_message';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;


    public function user(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'user_id', 'id');
    }

    public function recipient(): BelongsTo
    {
        return $this->belongsTo(\app\admin\model\User::class, 'recipient_id', 'id');
    }
}