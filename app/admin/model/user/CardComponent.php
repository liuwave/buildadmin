<?php

namespace app\admin\model\user;

use Throwable;
use think\Model;

/**
 * CardComponent
 *
 * @property int $id ID
 * @property int $status 状态:0=禁用,1=启用
 * @property int $weigh 权重
 * @property string $component 组件名称
 * @property string $create_time 创建时间
 * @property string $position 位置:statistic=个性签名之下-统计信息(inline),under_motto=个性签名之下-独占行(block),opt=统计信息之下-操作按钮(inline),middle=会员信息版块与选项卡版块之间(block),tab=作为底部选项卡之一(block)
 * @property string $title 标题
 */
class CardComponent extends Model
{
    // 表名
    protected $name = 'user_card_component';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    /**
     * 入库前
     * @throws Throwable
     */
    protected static function onAfterInsert($model)
    {
        if ($model->weigh == 0) {
            $pk = $model->getPk();
            if (strlen($model[$pk]) >= 19) {
                $model->where($pk, $model[$pk])->update(['weigh' => $model->count()]);
            } else {
                $model->where($pk, $model[$pk])->update(['weigh' => $model[$pk]]);
            }
        }
    }
}