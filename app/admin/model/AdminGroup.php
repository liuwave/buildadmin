<?php

namespace app\admin\model;

use think\Model;

/**
 * AdminGroup模型
 *
 * @property int $id ID
 * @property int $pid 上级分组
 * @property string $create_time 创建时间
 * @property string $name 组名
 * @property string $rules 权限规则ID
 * @property string $status 状态:0=禁用,1=启用
 * @property string $update_time 更新时间
 */
class AdminGroup extends Model
{
    protected $autoWriteTimestamp = true;
}