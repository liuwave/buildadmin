<?php

namespace app\admin\model\llshop;

use think\Model;

/**
 * Test
 */
class Test extends Model
{
    // 表名
    protected $name = 'llshop_test';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $createTime = false;


    public function llshopGoodsLink(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(\app\admin\model\llshop\goods\Link::class, 'llshop_goods_link_id', 'id');
    }
}