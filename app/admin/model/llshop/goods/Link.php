<?php

namespace app\admin\model\llshop\goods;

use think\Model;

/**
 * Link
 */
class Link extends Model
{
    // 表名
    protected $name = 'llshop_goods_link';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

}