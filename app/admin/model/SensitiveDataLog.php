<?php

namespace app\admin\model;

use think\Model;
use think\model\relation\BelongsTo;

/**
 * SensitiveDataLog 模型
 *
 * @property int $admin_id 操作管理员
 * @property int $id ID
 * @property int $id_value 被修改项主键值
 * @property int $is_rollback 是否已回滚:0=否,1=是
 * @property int $sensitive_id 敏感数据规则ID
 * @property string $after 修改后
 * @property string $before 修改前
 * @property string $create_time 创建时间
 * @property string $data_comment 被修改项
 * @property string $data_field 被修改字段
 * @property string $data_table 数据表
 * @property string $ip 操作者IP
 * @property string $primary_key 数据表主键
 * @property string $useragent User-Agent
 * @property-read \app\admin\model\Admin $admin
 * @property-read \app\admin\model\SensitiveData $sensitive
 */
class SensitiveDataLog extends Model
{
    protected $name = 'security_sensitive_data_log';

    protected $autoWriteTimestamp = true;
    protected $updateTime         = false;

    public function sensitive(): BelongsTo
    {
        return $this->belongsTo(SensitiveData::class, 'sensitive_id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}