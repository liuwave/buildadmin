<?php

namespace app\admin\model;

use think\Model;

/**
 * DataRecycle 模型
 *
 * @property int $id ID
 * @property string $controller 控制器
 * @property string $controller_as 控制器别名
 * @property string $create_time 创建时间
 * @property string $data_table 对应数据表
 * @property string $name 规则名称
 * @property string $primary_key 数据表主键
 * @property string $status 状态:0=禁用,1=启用
 * @property string $update_time 更新时间
 */
class DataRecycle extends Model
{
    protected $name = 'security_data_recycle';

    protected $autoWriteTimestamp = true;
}