<?php

namespace app\admin\model;

use ba\Random;
use think\Model;
use think\model\relation\BelongsTo;

/**
 * User 模型
 *
 * @property int $gender 性别:0=未知,1=男,2=女
 * @property int $group_id 分组ID
 * @property int $id ID
 * @property int $join_time 加入时间
 * @property int $last_login_time 上次登录时间
 * @property int $login_failure 登录失败次数
 * @property int $money 余额
 * @property int $score 积分
 * @property string $avatar 头像
 * @property string $birthday 生日
 * @property string $create_time 创建时间
 * @property string $email 邮箱
 * @property string $join_ip 加入IP
 * @property string $last_login_ip 上次登录IP
 * @property string $mobile 手机
 * @property string $motto 签名
 * @property string $nickname 昵称
 * @property string $password 密码
 * @property string $salt 密码盐
 * @property string $status 状态
 * @property string $update_time 更新时间
 * @property string $username 用户名
 * @property-read \app\admin\model\UserGroup $group
 */
class User extends Model
{
    protected $autoWriteTimestamp = true;

    public function getAvatarAttr($value): string
    {
        return full_url($value, false, config('buildadmin.default_avatar'));
    }

    public function setAvatarAttr($value): string
    {
        return $value == full_url('', false, config('buildadmin.default_avatar')) ? '' : $value;
    }

    public function getMoneyAttr($value): string
    {
        return bcdiv($value, 100, 2);
    }

    public function setMoneyAttr($value): string
    {
        return bcmul($value, 100, 2);
    }

    /**
     * 重置用户密码
     * @param int|string $uid         用户ID
     * @param string     $newPassword 新密码
     * @return int|User
     */
    public function resetPassword(int|string $uid, string $newPassword): int|User
    {
        $salt   = Random::build('alnum', 16);
        $passwd = encrypt_password($newPassword, $salt);
        return $this->where(['id' => $uid])->update(['password' => $passwd, 'salt' => $salt]);
    }

    public function group(): BelongsTo
    {
        return $this->belongsTo(UserGroup::class, 'group_id');
    }
}