<?php
return [
    'name'                       => '模型名称',
    'table name'                 => '数据表名',
    'table name error'           => '数据表名错误，请以字母开头、可包含大小写a-z、数字0-9、_、最多63位',
    'Data table already exists!' => '数据表已经存在！',
];