<?php
return [
    'Message sending failed, please try again!' => '消息发送失败，请重试！',
    "The user can't be found!"                  => '用户找不到啦！',
    'Delete failed, please try again!'          => '删除失败，请重试！',
];
