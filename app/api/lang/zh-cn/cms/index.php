<?php
return [
    'title require'                                 => '标题不能为空',
    'link require'                                  => '链接不能为空',
    'logo require'                                  => 'LOGO不能为空',
    'Submission successful, please wait for review' => '提交成功，请等待审核',
];