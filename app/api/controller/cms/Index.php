<?php

namespace app\api\controller\cms;

use app\admin\library\module\Server;
use app\admin\model\cms\Config;
use app\admin\model\cms\ContentModel;
use app\admin\model\cms\ContentModelFieldConfig;
use app\admin\model\user\Recent;
use ba\Filesystem;
use ba\Tree;
use ba\Date;
use ba\ClickCaptcha;
use think\facade\Db;
use think\facade\Validate;
use app\admin\model\cms\Channel;
use app\admin\model\cms\Tags;
use modules\cms\library\Helper;
use app\admin\model\cms\Content;
use app\admin\model\cms\SearchLog;
use app\common\controller\Frontend;
use app\admin\model\cms\FriendlyLink;

class Index extends Frontend
{
    /**
     * @var ?Tree
     */
    protected ?Tree $tree = null;

    protected array $noNeedLogin = ['*'];

    protected bool $dbCacheEnabled = true;

    public function initialize(): void
    {
        parent::initialize();
        $this->tree = Tree::instance();
        $this->dbCacheEnabled = Helper::getDbCacheEnabled();
    }

    /**
     * CMS初始化接口
     */
    public function init()
    {
        // CMS配置
//        $cmsConfigArr = [];
//        $cmsConfigData = Db::name('cms_config')->select();
//        foreach ($cmsConfigData as $item) {
//            $cmsConfigArr[$item['name']] = $item['value'];
//        }

        $cmsConfigArr = Config::getCmsConfig();

        // 右栏数据配置-s
        $recommendChannel = Channel::cache($this->dbCacheEnabled, null, Channel::$cacheTag)
            ->where('status', 1)
            ->where('index_rec', '<>', '')
            ->order('weigh', 'desc')
            ->select()
            ->toArray();
        foreach ($recommendChannel as $item) {
            $channelIds = Helper::getChannelChildrenIds($item['id']);
            $channelIds[] = $item['id'];

            $recommendContent[] = [
                'recTitle' => $item['index_rec'],
                'content'  => Content::cacheAlways($this->dbCacheEnabled, 3600, Channel::$cacheTag)
                    ->where('status', 'normal')
                    ->where('channel_id', 'in', $channelIds)
                    ->where(function ($query) {
                        if (!$this->auth->isLogin()) {
                            $query->where('allow_visit_groups', 'all');
                        }
                    })
                    ->orderRaw("IF(flag LIKE '%top%', 1, 0) DESC")
                    ->order('weigh', 'desc')
                    ->limit(10)
                    ->field(['id', 'title', 'title_style', 'images', 'create_time', 'publish_time', 'channel_id'])
                    ->select()
                    ->toArray(),
            ];
        }

        // 右栏数据穿插的广告图片
        $rightSideBarAd = Helper::getBlock('right-side-bar');

        // 右栏热门标签
        $hotTags = Db::name('cms_tags')
            ->cache($this->dbCacheEnabled, 3600, Tags::$cacheTag)
            ->order([
                'weigh desc',
                'document_count desc',
            ])
            ->limit(30)
            ->select()
            ->toArray();

        // 组合右栏数据
        $rightSideBar = [];
        for ($i = 0; $i < 10; $i++) {
            if (isset($recommendContent[$i])) {
                $rightSideBar[] = [
                    'type'  => 'recommendContent',
                    'data'  => $recommendContent[$i]['content'],
                    'title' => $recommendContent[$i]['recTitle'],
                ];
            }
            if (isset($rightSideBarAd[$i])) {
                $rightSideBar[] = [
                    'type' => 'ad',
                    'data' => $rightSideBarAd[$i],
                ];
            }
            if (!isset($recommendContent[$i]) && !isset($rightSideBarAd[$i])) break;
        }

        array_splice($rightSideBar, 2, 0, [
            [
                'type' => 'tags',
                'data' => $hotTags,
            ]
        ]);

        $cmsConfigArr['right_sidebar'] = $rightSideBar;
        // 右栏数据-e

//        是否开启会员
        $interaction = Server::getIni(Filesystem::fsFit(root_path() . 'modules/interaction/'));
        $cmsConfigArr['interactionInstalled'] = $interaction && $interaction['state'] == 1;

        // 友情链接
        $cmsConfigArr['friendly_link'] = FriendlyLink::cache($this->dbCacheEnabled, 3600, FriendlyLink::$cacheTag)
            ->where('status', 'enable')
            ->order('weigh', 'desc')
            ->select();

        $this->success('', [
            'config' => $cmsConfigArr,
        ]);
    }

    /**
     * 首页
     */
    public function index()
    {
        // 标记最新的文章
//        $newContent = Content::cache($this->dbCacheEnabled, 3600, Content::$cacheTag)
//            ->where('status', 'normal')
//            ->where(function ($query) {
//                if (!$this->auth->isLogin()) {
//                    $query->where('allow_visit_groups', 'all');
//                }
//            })
//            ->where('flag', 'find in set', 'new')
//            ->orderRaw("IF(flag LIKE '%top%', 1, 0) DESC")
//            ->order('weigh', 'desc')
//            ->field(['id', 'title', 'title_style', 'images', 'tags', 'create_time', 'publish_time', 'description', 'channel_id', 'views', 'likes'])
//            ->limit(36)
//            ->select()
//            ->toArray();
//        if (count($newContent) % 2 != 0) {
//            array_pop($newContent);
//        }
//        foreach ($newContent as &$item) {
//            $item['create_time'] = Date::human($item['create_time']);
//        }

        // 按更新时间排序的文章
        $newPublishContent = Content::cache($this->dbCacheEnabled, 3600, Content::$cacheTag)
            ->where('status', 'normal')
            ->where(function ($query) {
                if (!$this->auth->isLogin()) {
                    $query->where('allow_visit_groups', 'all');
                }
            })
            ->append(['authorInfo','cmsChannel'])
            ->where('flag', 'not like', '%new%')
            ->orderRaw("IF(flag LIKE '%top%', 1, 0) DESC")
            ->order('publish_time desc,create_time desc')
            ->field(['id', 'title', 'title_style', 'images', 'create_time', 'publish_time', 'description', 'channel_id', 'views', 'likes','comments', 'user_id'])
            ->limit(16)
            ->select()
            ->toArray();
        if (count($newPublishContent) % 2 != 0) {
            array_pop($newPublishContent);
        }
        foreach ($newPublishContent as &$item) {
            $item['create_time'] = Date::human($item['create_time']);
        }

        // 封面频道前五
        $coverChannel = Db::name('cms_channel')
            ->cache($this->dbCacheEnabled, 86400, Channel::$cacheTag)
            ->where('type', 'cover')
            ->where('status', 1)
            ->order('weigh', 'desc')
            ->limit(5)
            ->select();

        $this->success('', [
            'indexTopBar'       => Helper::getBlock('index-top-bar'),
            'indexCarousel'     => Helper::getBlock('index-carousel'),
            'indexFocus'        => Helper::getBlock('index-focus'),
            'newContent'        => [],
            'newPublishContent' => $newPublishContent,
            'coverChannel'      => $coverChannel,
        ]);
    }

    /**
     * 文章列表、频道首页
     */
    public function articleList()
    {
        $tagId = $this->request->get('tag');
        $channelId = $this->request->get('channel');
        $userId = $this->request->get('user');
        $keywords = $this->request->get("keywords/s", '');
        $order = $this->request->get("order/s", '');
        $limit = $this->request->get("limit/d", 16);
        $filter = $this->request->get("filter/a", []);
        $template = $this->request->get("template/s", 'doubleColumnList');
        $breadcrumb = [];
        $where = [];
        $title = [];
        $info = [];
        $filterConfig = [];

        // 频道
        if ($channelId) {
            $info = Channel::where('id', $channelId)
                ->cache($this->dbCacheEnabled, null, Channel::$cacheTag)
                ->where('status', 1)
                ->find();
            if ($info) {
                if ($info['allow_visit_groups'] == 'user' && !$this->auth->isLogin()) {
                    $this->error(__('Please login first'), [
                        'routePath' => '/user/login'
                    ], 302);
                }
                if ($info['type'] == 'cover') {
                    // 封面频道需要不同的数据
                    $this->coverChannel($channelId, $info);
                }
                $title[] = $info['name'];
                $template = $template == 'doubleColumnList' ? $info['template'] : $template;

                // 通过对应模型，取得前端筛选数据
                $modelInfo = Db::name('cms_content_model')
                    ->cache($this->dbCacheEnabled, null, ContentModel::$cacheTag)
                    ->where('id', $info['content_model_id'])
                    ->where('status', 1)
                    ->find();
                if ($modelInfo) {
                    $filterConfig = Db::name('cms_content_model_field_config')
                        ->cacheAlways($this->dbCacheEnabled, null, ContentModelFieldConfig::$cacheTag)
                        ->field('id,name,title,type,frontend_filter_dict')
                        ->where('content_model_id', $modelInfo['id'])
                        ->where('frontend_filter', 1)
                        ->where('status', 1)
                        ->order('weigh desc')
                        ->select()
                        ->toArray();
                    foreach ($filterConfig as &$item) {
                        $item['frontend_filter_dict'] = str_attr_to_array($item['frontend_filter_dict']);
                        if ($filter && array_key_exists($item['name'], $filter) && array_key_exists($filter[$item['name']], $item['frontend_filter_dict'])) {
                            $where[] = [$item['name'], '=', $filter[$item['name']]];
                        }
                    }
                }
            }

            // 同时查询子频道内容
            $channelIds = Helper::getChannelChildrenIds($channelId);
            $channelIds[] = $channelId;
            $where[] = ['channel_id', 'in', $channelIds];

            // 面包屑
            $breadcrumb = Helper::getParentChannel($channelId);
            foreach ($breadcrumb as &$item) {
                $item['type'] = 'channel';
            }
            $breadcrumb = array_reverse($breadcrumb);
        }
        // 标签
        if ($tagId) {
            $where[] = ['tags', 'find in set', $tagId];

            $info = Tags::where('id', $tagId)
                ->cache($this->dbCacheEnabled, 3600, Tags::$cacheTag)
                ->where('status', 1)
                ->find();
            if ($info) {
                $title[] = $info['name'];
                $info['type'] = 'tag';
                $breadcrumb[] = $info;
            }
        }
        if ($userId) {
            $where[] = ['user_id', '=', $userId];

            $info = Db::name('user')->field('id,nickname as name,avatar,gender')->where('status', 'enable')->find();
            if ($info) {
                $title[] = $info['name'];
                $info['type'] = 'user';
                $breadcrumb[] = $info;
            }
        }
        // 关键词
        if ($keywords) {
            $where[] = ['title|description', 'like', "%{$keywords}%"];
            $title[] = $keywords;
            $breadcrumb[] = ['name' => $keywords, 'type' => 'search'];

            // 记录关键词
            $searchLog = SearchLog::where('search', $keywords)->find();
            if ($searchLog) {
                $searchLog->inc('count')->update();
            } else {
                SearchLog::create([
                    'user_id' => $this->auth->isLogin() ? $this->auth->id : 0,
                    'search'  => $keywords,
                    'count'   => 1,
                    'hot'     => 0,
                    'status'  => 0,
                ]);
            }
        }

        // 排序
        if ($order) {
            $order = explode(',', $order);
            if (isset($order[0]) && isset($order[1]) && ($order[1] == 'asc' || $order[1] == 'desc')) {
                $order = [$order[0] => $order[1]];
            }
        } else {
            $order = ['weigh' => 'desc'];
        }

        $content = Content::where('status', 'normal')
            ->cache($this->dbCacheEnabled, 3600, Content::$cacheTag)
            ->where($where)
            ->where(function ($query) {
                if (!$this->auth->isLogin()) {
                    $query->where('allow_visit_groups', 'all');
                }
            })
            ->field(['id', 'title', 'title_style', 'images', 'create_time', 'publish_time', 'description', 'views', 'likes'])
            ->order($order)
            ->paginate($limit)->each(function ($item) {
                $item->create_time = Date::human($item->create_time);
            });


        // 多个标题，加上双引号
        if (count($title) > 1) {
            foreach ($title as &$item) {
                $item = '“' . $item . '”';
            }
        }

        $this->success('', [
            'list'         => $content->items(),
            'total'        => $content->total(),
            'info'         => $info,
            'title'        => $title,
            'template'     => $template,
            'breadcrumb'   => $breadcrumb,
            'filterConfig' => $filterConfig,
        ]);
    }

    /**
     * 封面频道
     */
    public function coverChannel($id, $info = [])
    {
        if (!$info) {
            $info = Channel::where('id', $id)
                ->cache($this->dbCacheEnabled, null, Channel::$cacheTag)
                ->where('status', 1)
                ->where(function ($query) {
                    if (!$this->auth->isLogin()) {
                        $query->where('allow_visit_groups', 'all');
                    }
                })
                ->find();
        }
        if (!$info) {
            $this->error('频道不存在');
        }
        $focus = Helper::getBlock($info['template'] . '-focus');
        $carousel = Helper::getBlock($info['template'] . '-carousel');

        // 获取子栏目数据
        $children = Db::name('cms_channel')
            ->cache($this->dbCacheEnabled, null, Channel::$cacheTag)
            ->where('pid', $id)
            ->order('weigh', 'desc')
            ->select()
            ->toArray();
        $content = [];
        foreach ($children as $child) {
            $channelIds = Helper::getChannelChildrenIds($child['id']);
            $channelIds[] = $child['id'];
            $contentList = Content::where('status', 'normal')
                ->cache($this->dbCacheEnabled, 3600, Channel::$cacheTag)
                ->where('channel_id', 'in', $channelIds)
                ->where(function ($query) {
                    if (!$this->auth->isLogin()) {
                        $query->where('allow_visit_groups', 'all');
                    }
                })
                ->orderRaw("IF(flag LIKE '%top%', 1, 0) DESC")
                ->field(['id', 'channel_id', 'title', 'title_style', 'images', 'create_time', 'publish_time', 'description', 'views', 'likes'])
                ->order('weigh', 'desc')
                ->limit(7)
                ->select()
                ->toArray();
            if ($contentList) {
                foreach ($contentList as &$item) {
                    $item['create_time'] = Date::human($item['create_time']);
                }
                $content[] = [
                    'info'        => $child,
                    'contentList' => $contentList,
                ];
            }
        }

        $this->success('', [
            'info'     => $info,
            'focus'    => $focus,
            'carousel' => $carousel,
            'template' => $info['template'],
            'content'  => $content,
        ]);
    }

    /**
     * 继续加载新发布文章
     */
    public function getNewPublish()
    {
        $page = $this->request->get('page', 2);
        $channelId = $this->request->get('channel_id/d', 0);
        $where = [];

        if ($channelId > 0) {
            // 同时查询子频道内容
            $channelIds = Helper::getChannelChildrenIds($channelId);
            $channelIds[] = $channelId;
            $where[] = ['channel_id', 'in', $channelIds];
        }


        // 按更新时间排序的文章
        $newPublishContent = Content::where('status', 'normal')
            ->cache($this->dbCacheEnabled, 3600, Content::$cacheTag)
            ->where($where)
            ->where(function ($query) {
                if (!$this->auth->isLogin()) {
                    $query->where('allow_visit_groups', 'all');
                }
            })
            ->where('flag', 'not like', '%new%')
            ->orderRaw("IF(flag LIKE '%top%', 1, 0) DESC")
            ->order('publish_time desc,create_time desc')
            ->append(['authorInfo','cmsChannel'])
            ->field(['id', 'title', 'title_style', 'images', 'create_time', 'publish_time', 'description', 'channel_id', 'views', 'likes','comments', 'user_id'])
            ->limit(($page - 1) * 16, 16)
            ->select()
            ->toArray();
        if (count($newPublishContent) % 2 != 0) {
            array_pop($newPublishContent);
        }
        foreach ($newPublishContent as &$item) {
            $item['create_time'] = Date::human($item['create_time']);
        }
        $this->success('', [
            'newPublishContent' => $newPublishContent,
        ]);
    }

    public function applyFriendlyLink()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $validate = Validate::rule([
                'title'       => 'require',
                'link'        => 'require',
                'logo'        => 'require',
                'captchaId'   => 'require',
                'captchaInfo' => 'require'
            ])->message([
                'title'       => 'title require',
                'link'        => 'link require',
                'logo'        => 'logo require',
                'captchaId'   => 'Captcha error',
                'captchaInfo' => 'Captcha error',
            ]);
            if (!$validate->check($data)) {
                $this->error(__($validate->getError()));
            }

            $clickCaptcha = new ClickCaptcha();
            if (!$clickCaptcha->check($data['captchaId'], $data['captchaInfo'])) {
                $this->error(__('Captcha error'));
            }

            $data['user_id'] = $this->auth->id;
            $data['status'] = 'pending_trial';
            $data['remark'] = $data['remark'] . ($data['contact'] ? ' 联系人：' . $data['contact'] : '');
            FriendlyLink::create($data);
            $this->success(__('Submission successful, please wait for review'));
        }
        $this->error();
    }

    /**
     * 搜索页数据（非搜索）
     */
    public function search()
    {
        $hotKeywords = SearchLog::where('hot', 1)
            ->where('status', 1)
            ->order('weigh', 'desc')
            ->select();
        foreach ($hotKeywords as $key => $hotKeyword) {
            $hotKeywords[$key]['rank'] = $key + 1;
        }
        $this->success('', [
            'hotKeywords' => $hotKeywords
        ]);
    }

    /**
     * 标签下拉
     */
    public function tags()
    {
        $limit = $this->request->get("limit/d", 10);
        $quickSearch = $this->request->get("quick_search/s", '');
        $initValue = $this->request->get("initValue/a", '');
        $where = [];

        // 快速搜索
        if ($quickSearch) {
            $where[] = ['name', 'LIKE', "%{$quickSearch}%"];
        }
        if ($initValue) {
            $where[] = ['id', 'in', $initValue];
            $limit = 999999;
        }

        $res = Db::name('cms_tags')
            ->where($where)
            ->where('status', 1)
            ->order('id desc')
            ->paginate($limit);

        $this->success('', [
            'list'  => $res->items(),
            'total' => $res->total(),
        ]);
    }

    /**
     * 频道下拉
     */
    public function channel()
    {
        $quickSearch = $this->request->get("quick_search/s", '');
        $initValue = $this->request->get("initValue/a", '');
        $where = [];

        // 快速搜索
        if ($quickSearch) {
            $where[] = ['name', 'LIKE', "%{$quickSearch}%"];
        }
        if ($initValue) {
            $where[] = ['id', 'in', $initValue];
        }

        $res = Db::name('cms_channel')
            ->where($where)
            ->where('frontend_contribute', 1)
            ->where('status', 1)
            ->order('weigh desc')
            ->select()
            ->toArray();
        $res = $this->tree->assembleTree($this->tree->getTreeArray($this->tree->assembleChild($res)));

        $this->success('', [
            'list' => $res,
        ]);
    }

    public function autoPublish()
    {


        $result = Content::where('status', 'unaudited')
            ->where('create_time', '<', time())
            ->field(['id', 'title', 'create_time', 'user_id', 'publish_time', 'status'])
            ->select()
            ->each(function (Content $content) {
                $content->status = 'normal';


                $recentHtml = '发表了内容 <a href="/info/' . $content->id . '">' . $content['title'] . '</a>';
                Recent::create([
                    'user_id'      => $content->getData('user_id'),
                    'content'      => $recentHtml,
                    'pure_content' => $content->getData('title'),
                    'source_type'  => 'publish_content',
                    'source_id'    => $content->id,
                    'create_time'  => $content->getData('publish_time')
                ]);
                $content->save();
            });

//        $result = Db::name('cms_content')
//            ->where('status', 'unaudited')
//            ->where('create_time', '<', time())
//            ->update([
//                'status' => 'normal',
//            ]);
        $this->generateSitemap();
        $this->success('', [
            'count' => $result->count()
        ]);
    }

    protected function generateSitemap(): void
    {
        $sitemaps = [];

        if (cache('has_sitemaps')) {
            return;
        }


        $baseUrl = Config::getCmsConfig('front_host');

        $sitemaps[] = [
            'loc'        => $baseUrl,
            'priority'   => 1,
            'lastmod'    => date('Y-m-d'),
            'changefreq' => 'Monthly'
        ];


        Channel::cache($this->dbCacheEnabled, null, Channel::$cacheTag)
            ->where('status', 1)
            ->field(['id', 'update_time'])
            ->select()
            ->each(function ($channel) use (&$sitemaps, $baseUrl) {
                $sitemaps[] = [
                    'loc'        => $baseUrl . '/channel/' . $channel->id,
                    'priority'   => 0.8,
                    'lastmod'    => date('Y-m-d'),
                    'changefreq' => 'Monthly'
                ];
            });
        Content::cacheAlways($this->dbCacheEnabled, 3600)
            ->where('status', 'normal')
            ->field(['id', 'create_time', 'publish_time'])
            ->select()
            ->each(function ($content) use (&$sitemaps, $baseUrl) {
                $timeStr = $content->publish_time > 0 ? date('Y-m-d', (int)($content->publish_time)) : date('Y-m-d');
                $sitemaps[] = [
                    'loc'        => $baseUrl . '/info/' . $content->id,
                    'priority'   => 0.7,
                    'lastmod'    => $timeStr,
                    'changefreq' => 'Monthly'
                ];
            });

        $sitemapXmlArray = array_map(function ($item) {
            return <<<IXML
<url>
    <loc>{$item['loc']}</loc>
    <priority>{$item['priority']}</priority>
    <lastmod>{$item['lastmod']}</lastmod>
    <changefreq>{$item['changefreq']}</changefreq>
</url>
IXML;
        }, $sitemaps);

        $xmlUrls = implode(PHP_EOL, $sitemapXmlArray);
        $xml =
            <<<EHO
<?xml version="1.0" encoding="UTF-8"?>
<urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{$xmlUrls}
</urlset>
EHO;
        $publicPath = app()->getRootPath() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR;
        @file_put_contents($publicPath . 'sitemap.xml', $xml);

        $sitemapTextArray = array_map(function ($item) {
            return $item['loc'];
        }, $sitemaps);


        $textUrls = implode(PHP_EOL, $sitemapTextArray);
        @file_put_contents($publicPath . 'sitemap.txt', $textUrls);

        chmod($publicPath . 'sitemap.xml', 0777);
        chmod($publicPath . 'sitemap.txt', 0777);

        cache('has_sitemaps', 1, 3600);

    }
}