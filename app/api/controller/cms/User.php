<?php

namespace app\api\controller\cms;

use app\admin\model\cms\Config;
use app\admin\model\cms\Tags;
use ba\Date;
use ba\Tree;
use modules\cms\library\Helper;
use ParseDownExt;
use think\facade\Db;
use app\common\controller\Frontend;
use Throwable;

class User extends Frontend
{
    public function collect()
    {
        $limit = $this->request->request('limit');

        $res = Db::name('cms_statistics')
            ->field('c.id,c.title,c.title_style,c.description,c.images,c.publish_time,c.create_time,c.channel_id,c.views,ch.name')
            ->alias('s')
            ->join('cms_content c', 's.content_id=c.id')
            ->join('cms_channel ch', 'c.channel_id=ch.id')
            ->where('s.user_id', $this->auth->id)
            ->where('s.type', 2)
            ->order('s.create_time', 'desc')
            ->paginate($limit)->each(function ($item) {
                $item['publish_time'] = Date::human($item['publish_time'] ?? $item['create_time']);
                $item['title_style']  = \app\admin\model\cms\Content::getTitleStyleAttr($item['title_style']);
                return $item;
            });

        $this->success('', [
            'list'  => $res->items(),
            'total' => $res->total(),
        ]);
    }

    public function order()
    {
        $limit = $this->request->request('limit');
        $res   = Db::name('cms_pay_log')
            ->field('p.*, c.title, c.images, c.description')
            ->alias('p')
            ->join('cms_content c', 'p.object_id=c.id', 'LEFT')
            ->where('p.user_id', $this->auth->id)
            ->where('p.project', 'content')
            ->where('p.pay_time', 'not null')
            ->order('p.pay_time', 'desc')
            ->paginate($limit)->each(function ($item) {
                $item['pay_time'] = Date::human($item['pay_time']);
                return $item;
            });
        $this->success('', [
            'list'  => $res->items(),
            'total' => $res->total(),
        ]);
    }

    public function comment()
    {
//        $commentLanguage = Db::name('cms_config')
//            ->where('name', 'comment_language')
//            ->value('value');

        $commentLanguage=Config::getCmsConfig('comment_language');

        $parseDown       = new ParseDownExt();
        $parseDown->setSafeMode(true);

        $limit = $this->request->request('limit');
        $res   = Db::name('cms_comment')
            ->alias('ct')
            ->field('ct.*, c.title')
            ->join('cms_content c', 'ct.content_id=c.id', 'LEFT')
            ->where('ct.user_id', $this->auth->id)
            ->where('ct.type', 'content')
            ->order('ct.weigh desc,ct.create_time desc')
            ->paginate($limit)->each(function ($item) use ($commentLanguage, $parseDown) {
                $item['content'] = !$item['content'] ? '' : htmlspecialchars_decode($item['content']);
                if ($commentLanguage == 'markdown') {
                    $item['content'] = $parseDown->text($item['content']);
                }
                $item['create_time'] = Date::human($item['create_time']);
                return $item;
            });
        $this->success('', [
            'list'  => $res->items(),
            'total' => $res->total(),
        ]);
    }

    public function content()
    {
        $limit = $this->request->request('limit');
        $res   = Db::name('cms_content')
            ->alias('c')
            ->field('c.id,c.title,c.title_style,c.images,c.price,c.publish_time,c.create_time,c.channel_id,c.views,c.comments,c.likes,c.status,c.memo,ch.name as channelName')
            ->join('cms_channel ch', 'c.channel_id=ch.id', 'LEFT')
            ->where('user_id', $this->auth->id)
            ->order('create_time desc')
            ->paginate($limit)->each(function ($item) {
                $item['publish_time'] = Date::human($item['publish_time'] ?? $item['create_time']);
                $item['title_style']  = \app\admin\model\cms\Content::getTitleStyleAttr($item['title_style']);
                $item['sales']        = Db::name('cms_pay_log')
                    ->where('project', 'content')
                    ->where('object_id', $item['id'])
                    ->count();
                $item['images']       = \app\admin\model\cms\Content::getImagesAttr($item['images']);
                return $item;
            });

        $this->success('', [
            'list'  => $res->items(),
            'total' => $res->total(),
        ]);
    }

    public function delContent()
    {
        $id = $this->request->request('id');

        $modelInfo = Db::name('cms_content')
            ->alias('co')
            ->field('co.id,ch.id,cm.*')
            ->join('cms_channel ch', 'co.channel_id=ch.id')
            ->join('cms_content_model cm', 'ch.content_model_id=cm.id')
            ->where('co.id', $id)
            ->where('co.user_id', $this->auth->id)
            ->find();
        if (!$modelInfo) {
            $this->error('文章数据异常！');
        }

        Db::name('cms_content')
            ->where('id', $id)
            ->where('user_id', $this->auth->id)
            ->delete();
        Db::name($modelInfo['table'])->where('id', $id)->delete();

        $this->success();
    }

    public function buyLog()
    {
        $id    = $this->request->request('id');
        $limit = $this->request->request('limit');
        $res   = Db::name('cms_pay_log')
            ->alias('p')
            ->field('p.*,u.nickname')
            ->join('user u', 'p.user_id=u.id', 'LEFT')
            ->where('p.object_id', $id)
            ->where('p.project', 'content')
            ->where('p.pay_time', 'not null')
            ->order('p.pay_time', 'desc')
            ->paginate($limit)
            ->each(function ($item) {
                $item['pay_time'] = Date::human($item['pay_time']);
                return $item;
            });
        $this->success('', [
            'list'  => $res->items(),
            'total' => $res->total(),
        ]);
    }

    public function getContributeChannel(): array
    {
        $channel = Db::name('cms_channel')
            ->where('frontend_contribute', 1)
            ->where('status', 1)
            ->select();
        $tree    = Tree::instance();
        return $tree->assembleTree($tree->getTreeArray($tree->assembleChild($channel->toArray())));
    }

    public function contributeChannel()
    {
        $this->success('', [
            'channel' => $this->getContributeChannel()
        ]);
    }

    public function contribute()
    {
        $id        = $this->request->param('id');
        $channelId = $this->request->param('channel_id');

        $contentModel = new \app\admin\model\cms\Content;

        $info = false;
        if ($id) {
            $info      = $contentModel::where('id', $id)
                ->where('user_id', $this->auth->id)
                ->find();
            $channelId = $info->channel_id ?? $channelId;
        }

        if (!$channelId) {
            $this->error('请选择投稿频道！');
        }

        $modelInfo = Db::name('cms_content_model')
            ->alias('m')
            ->field('m.*,ch.frontend_contribute,ch.status as ch_status')
            ->join('cms_channel ch', 'm.id=ch.content_model_id')
            ->where('ch.id', $channelId)
            ->where('m.status', 1)
            ->find();
        if (!$modelInfo) {
            $this->error('频道信息错误，请联系网站管理员！');
        }
        // 已经投递的稿件，不检测频道状态
        if (!$info && $modelInfo['frontend_contribute'] == 0) {
            $this->error('频道投稿已关闭，请重新选择频道！');
        }
        if (!$info && $modelInfo['ch_status'] == 0) {
            $this->error('频道已被禁用，请重新选择频道！');
        }

        $fieldsConfig      = [];
        $attachedFields    = Helper::getTableFields($modelInfo['table']);
        $modelFieldsConfig = Db::name('cms_content_model_field_config')
            ->field('id,name,title,tip,rule,input_extend,extend,type')
            ->where('content_model_id', $modelInfo['id'])
            ->where('frontend_contribute', 1)
            ->where('status', 1)
            ->order(['weigh' => 'asc', 'id' => 'asc'])
            ->select()
            ->toArray();
        foreach ($modelFieldsConfig as $item) {
            if ($item['name'] == 'id') continue;
            $item['main_field'] = true;
            if (array_key_exists($item['name'], $attachedFields)) {
                $item['main_field']   = false;
                $item['extend']       = str_attr_to_array($item['extend']);
                $item['input_extend'] = str_attr_to_array($item['input_extend']);
                $item['default']      = Helper::restoreDefault($attachedFields[$item['name']]['COLUMN_DEFAULT'], $item['type']);
                $item['content']      = Helper::restoreDict($attachedFields[$item['name']]['COLUMN_COMMENT']);
            }
            $fieldsConfig[$item['name']] = $item;
        }

        if ($this->request->isPost()) {
            $this->request->filter('clean_xss');
            $data = $this->request->post();
            if (!$data) {
                $this->error(__('Parameter %s can not be empty', ['']));
            }
            $modelTableData = [];
            foreach ($data as $key => $datum) {
                if ($key == 'id') continue;
                if (array_key_exists($key, $attachedFields)) {
                    $modelTableData[$key] = $contentModel::setModelTableData($datum, isset($fieldsConfig[$key]) ? $fieldsConfig[$key]['type'] : '');
                }
            }

            $result = false;
            Db::startTrans();
            try {
                $data['status']  = 'unaudited';
                $data['user_id'] = $this->auth->id;

                // 自动新建标签
                if (isset($data['tags'])) {
                    $data['tags'] = \modules\cms\library\Helper::autoCreateTags($data['tags']);
                    foreach ($data['tags'] as $tag) {
                        Tags::where('id', $tag)->inc('document_count')->update();
                    }
                }

                if ($info) {
                    unset($data['status']);
                    $result = $info->save($data);
                    Db::name($modelInfo['table'])->where('id', $info->id)->update($modelTableData);
                } else {
                    unset($data['id']);
                    $result               = $contentModel::create($data);
                    $modelTableData['id'] = $result->id;
                    Db::name($modelInfo['table'])->insert($modelTableData);
                }
                Db::commit();
            } catch (Throwable $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }

            if ($result) {
                $this->success('提交成功，正在回到列表...');
            } else {
                $this->error('保存失败，请重试！');
            }
        }

        if ($info) {
            $modelTableData = Db::name($modelInfo['table'])->find($info->id);
            if ($modelTableData) {
                foreach ($modelTableData as $key => $modelTableDatum) {
                    if ($key == 'id') continue;
                    $info->$key = $contentModel::getModelTableData($modelTableDatum, isset($fieldsConfig[$key]) ? $fieldsConfig[$key]['type'] : '');
                }
            }
        }

        $this->success('', [
            'info'    => $info,
            'fields'  => $fieldsConfig,
            'channel' => $this->getContributeChannel(),
        ]);
    }
}